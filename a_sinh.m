function z=a_sinh(x,delta)
sum=x;
n=0;
while 1/3*abs(x.^(2*n+1)/factorial(2*n+1))>=delta
n=n+1;
sum=sum+(x.^(2*n+1)/factorial(2*n+1));
end
z=sum;
end