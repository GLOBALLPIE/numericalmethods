function z=a_cosh(x,delta)
sum=1;
n=0;
while 2/3*abs(x.^(2*n)/factorial(2*n))>=delta
n=n+1;
sum=sum+(x.^(2*n)/factorial(2*n));
end
z=sum;
end