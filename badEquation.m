function z=badEquation(n,U,E,N)
A=zeros(n);
b=zeros(n,1);
X=zeros(n,1);
for i=1:n
    A(i,i)=1+U*N*1;
    b(i)=-1;
    for g=1:i-1
        A(i,g)=U*N*1;
    end
    for j=i+1:n
        A(i,j)=-1+U*N*(-1);
    end
end
b(n)=1+E;
%vA=norm(A,inf)*norm(inv(A),inf)
z=[A,b];
end