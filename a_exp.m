function z=a_exp(x,delta)
sum=1;
n=0;
while abs(x.^n/factorial(n))>=delta
n=n+1;
sum=sum+x.^n/factorial(n);
end
z=sum;
end