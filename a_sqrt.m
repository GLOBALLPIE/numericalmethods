function z=a_sqrt(a,delta)
w1=1;
w2=0.5*(w1+a./w1);
while abs(w2-w1)>delta
w1=w2;
w2=0.5*(w1+a./w1);
end
z=w2;
end