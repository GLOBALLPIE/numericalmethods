function z=inversedProblem(f,fu,fv,a,b,E)
nf=matlabFunction(f);
nu=matlabFunction(fu);
nv=matlabFunction(fv);
syms u v
u_borders=[nu(fminbnd(nu,a,b)) nu(fminbnd(matlabFunction(-fu),a,b))];
v_borders=[nv(fminbnd(nv,a,b)) nv(fminbnd(matlabFunction(-fv),a,b))];
lb=[u_borders(1) v_borders(1)];
ub=[u_borders(2) v_borders(2)];

diffu=-abs(diff(f,u))
dfdu=matlabFunction(diffu)
dfdu=@(x)dfdu(x(1),x(2))
diffv=-abs(diff(f,v));
dfdv=matlabFunction(diffv);
dfdv=@(x)dfdv(x(1),x(2))
c1=-dfdu(fmincon(dfdu,[(u_borders(1)+u_borders(2))/2,(v_borders(1)+v_borders(2))/2],[],[],[],[],lb,ub))
c2=-dfdv(fmincon(dfdv,[(u_borders(1)+u_borders(2))/2,(v_borders(1)+v_borders(2))/2],[],[],[],[],lb,ub))
z=[E/(3*c1) E/(3*c2) E/3];
end