function y = minFunc(cb,a,b,eps)
    d2=eps/10;
    x_min=a;
    while abs(b-a)>eps
        x_min = (b+a)/2;
        fp=subs(cb,x_min+d2);
        fm=subs(cb,x_min-d2);
        if fp>fm
            a=x_min-d2
        else
            b=x_min+d2
        end
    end
    y=x_min;
  
end