function z=gausMethod(A,b)

s=size(A);
n=s(1);
currentString=1;
%coef=A(1,1);
%������ ���
for i=1:n
%���� ����������� �� ������ ������� � ������� �������
[coef,currentString]=max(abs(A(i:n,i)));
currentString=currentString+i-1;
%disp(A(i:n,i));
if coef==0
    continue
end
temp=A(i,:);
A(i,:)=A(currentString,:);
A(currentString,:)=temp;

tempB=b(i);
b(i)=b(currentString);
b(currentString)=tempB;
%����� ������ �������� �� �����������
coef=A(i,i);%���������
A(i,:)=A(i,:)/coef;
b(i)=b(i)/coef;
%����������� ���������, ������� ���� ��������
for j=i+1:n
    currentCoef=A(j,i);
A(j,:)=A(j,:)-A(i,:)*currentCoef;
b(j)=b(j)-b(i)*currentCoef;
end
end
x=zeros(n,1);
x(n)=b(n);
%fprintf('100: %f\n',x(n));
for i=n-1:-1:1
    for j=i+1:n
        x(i)=x(i)-A(i,j)*x(j);
    end
    x(i)=x(i)+b(i);
    %fprintf('%d: %f\n',i,x(i));
end
z=x;
end