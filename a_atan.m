function z=a_atan(x,delta)%Потестировать
if abs(x)<1
u=@(n)(-1)^n*x.^(2*n+1)/(2*n+1);
sum=x;
else
u=@(n)(-1)^(n+1)*x.^(-2*n-1)/(2*n+1);
sum=pi/2*sign(x)-x.^(-1);
end
n=0;
while abs(u(n))>=delta
n=n+1;
sum=sum+u(n);
end
z=sum;
end