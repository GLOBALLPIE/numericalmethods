function z=diagonalDominance(A)
s=size(A);
n=s(1);
for i=1:n
    %A(i,i)-(sum(abs(A(i,:)))-abs(A(i,i)))
    if A(i,i)-(sum(abs(A(i,:)))-abs(A(i,i)))<=0
        z=false;
        return
    end
end
z=true;
end