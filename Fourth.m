clc
clear
syms x y
F=[cos(x+0.5)+y-0.8;sin(y)-2*x-1.6]
E=0.0001;
F1=matlabFunction(F(1));
F2=matlabFunction(F(2));
figure;
hold on
fimplicit(F1);
fimplicit(F2);
res=newtonMethodForSystems(F,0.9,-0.4,E)
plot( res(1),res(2),'*');
grid on
hold off