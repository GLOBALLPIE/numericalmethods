function z=newtonMethod(f,a,b,beginPoint,E)

fa=matlabFunction(f);
dfdx=matlabFunction(diff(f));
d2fdxx=matlabFunction(diff(diff(f)));

if fa(beginPoint)*d2fdxx(beginPoint)<=0
    x0=(a+b)/2
else
x0=beginPoint;
end
x1=x0-fa(x0)/dfdx(x0);
while abs(x1-x0)>=E
        x0=x1;
        x1=x1-fa(x1)/dfdx(x1)
end
z=x1;
end