function z=a_cos(x,delta)
sum=1;
n=0;
while abs(x.^(2*n)/factorial(2*n))>=delta
n=n+1;
sum=sum+(-1)^n*(x.^(2*n)/factorial(2*n));
end
z=sum;
end