function z=zeidelMethod(fA,fb)


if ~positiveDetermined(fA)
    A=fA'*fA;
    b=fA'*fb;
else
    A=fA;
    b=fb;
end

s=size(A);
n=s(1);
C=zeros(n);
d=zeros(n,1);

for i=1:n
    for j=1:n
        if i==j
            continue
        end
    C(i,j)=-A(i,j)/A(i,i);
    end
    d(i)=b(i)/A(i,i);
end

x0=d;
x1=zeros(n,1);

while 1
for i=1:n
    for j=1:i-1
        x1(i)=x1(i)+C(i,j)*x1(j);
    end
    for j=i:n
        x1(i)=x1(i)+C(i,j)*x0(j);
    end
    x1(i)=x1(i)+d(i);
end
if norm(x1-x0)<10^-6
    break
end
x0=x1;
x1=zeros(n,1);
end

z=x1;

end