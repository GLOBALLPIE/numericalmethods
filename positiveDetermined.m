function z=positiveDetermined(A)
s=size(A);
n=s(1);
if issymmetric(A)
    Eig=eig(A);
else
    Eig=eig((A+A')/2);
end

%Eig=eig(A);
for i=1:n
        if real(Eig(i))<=0
            z=false;
            return
        end
end
    z=true;
end