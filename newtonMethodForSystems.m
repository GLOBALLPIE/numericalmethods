function z=newtonMethodForSystems(F,X,Y,E)
syms x y
x0=[X;Y];
dFdX=[diff(F(1),x),diff(F(1),y);diff(F(2),x),diff(F(2),y)];
%inv_dFadX=matlabFunction(inv(dFdX));
Fa=matlabFunction(F);
dFadX=matlabFunction(dFdX);
delta=gausMethod(dFadX(x0(1),x0(2)),-Fa(x0(1),x0(2)));
x1=x0+delta;
%x1=x0-inv_dFadX(x0(1),x0(2))*Fa(x0(1),x0(2))
while norm(x1-x0)>=E
x0=x1;
delta=gausMethod(dFadX(x0(1),x0(2)),-Fa(x0(1),x0(2)));
x1=x0+delta;
%x1=x1-inv_dFadX(x1(1),x1(2))*Fa(x1(1),x1(2));
end
z=x1;
end