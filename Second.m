clc
clear
format long
A=[13, 1, 1;
    1, 15, 1;
    1, 1, 17];
b=[15; 17;19];

gausMethod(A,b)
zeidelMethod(A,b)

U=10^-3;
E=10^-3;
n=4;

bad=badEquation(n,U,E,11);%n, U, E, N
gausRes=gausMethod(bad(:,1:n),bad(:,n+1))
zeidelRes=zeidelMethod(bad(:,1:n),bad(:,n+1))
norm(gausRes-zeidelRes)
%norm(gausRes-linsolve(bad(:,1:n),bad(:,n+1)))


%{
for i=1:n
    fprintf('zeidelX(%d): %.6f\n',i,zeidelRes(i));
    fprintf('gausX(%d): %.6f\n',i,gausRes(i));
    fprintf('delta(%d): %.6f\n\n',i,abs(gausRes(i)-zeidelRes(i)));
end
}
linsolve(bad(:,1:n),bad(:,n+1))
%}