clc
clear
format long

syms x y 

syms u v

X=[0.1:0.01:0.2];
E=10^-6;

f=@(arg)cosh(1+sqrt(1+arg)).*cos(sqrt(1+arg-arg.^2));
fg=u*v;
fu=cosh(x);
fv=cos(x);
complicated=inversedProblem3(fg,fu,fv,0.1,0.2,E);
arg1=1+a_sqrt(1+X,inversedProblem2(fu,0.1,0.2,complicated(1)));
arg2=a_sqrt(1+X-X.^2,inversedProblem2(fu,0.1,0.2,complicated(2)));
a_f=a_cosh(arg1,complicated(1)).*a_cos(arg2,complicated(2));
Result=[X',a_f',f(X)',abs(f(X)-a_f)']

%{
example=sqrt(sin(0.9*x+0.51))/(x*exp(x+0.3));
fu=sin(0.9*x+0.51);
fv=x*exp(x+0.3);
f=sqrt(u)/v;;

err=inversedProblem(f,fu,fv,0.5,0.6,E);
example=matlabFunction(example);
ap_sin=a_sin(0.9*X+0.51,err(1));
ap_exp=X.*a_exp(X+0.3,err(2));
ap_sqrt=a_sqrt(ap_sin,err(3).*ap_exp);
a_f=ap_sqrt./ap_exp;
Result=[X',a_f',example(X)',abs(example(X)-a_f)']
%}