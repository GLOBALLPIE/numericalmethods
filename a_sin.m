function z=a_sin(x,delta)
sum=x;
n=0;
while abs(x.^(2*n+1)/factorial(2*n+1))>=delta
n=n+1;
sum=sum+((-1)^n*x.^(2*n+1)/factorial(2*n+1));
end
z=sum;
end