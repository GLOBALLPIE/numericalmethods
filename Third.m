
clc
clear

syms x
a=-5;
b=5;
X=linspace(-20,20,500);
f=cot(x)-x.^2;

figure;
hold on
plot(X,cot(X), X,X.^2, X,X-X, X-X,X);
set(gca,'XLim',[a,b],'YLim',[-5,5],'XTick',linspace(a,b,20),'YTick',[-5:5]);
grid on
hold off
%}
newtonMethod(f,0.7,1.4,1,10^-4)