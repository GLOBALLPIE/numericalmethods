function z=inversedProblem3(f,fu,fv,a,b,E)%принцип равных погрешностей
nf=matlabFunction(f);
nu=matlabFunction(fu);
nv=matlabFunction(fv);

u_border=nu(fminbnd(matlabFunction(-fu),a,b));
v_border=nv(fminbnd(matlabFunction(-fv),a,b));
bu=v_border;
bv=u_border;
z=[E/(bu+bv),E/(bu+bv)];
end